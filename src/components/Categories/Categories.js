import React, {useState} from "react";


function Categories({items, onClickItem}) {
    const [activeItem, setState] = useState(null);

    const onSelectItem = index => {
        setState(index);
        onClickItem(index);
    };

    return (
        <div className="categories">
            <ul>
                <li
                    className={activeItem === null ? 'active' : ''}
                    onClick={() => onSelectItem(null)}
                >Всі
                </li>
                {items && items.map((el, index) => {
                    return <li
                        className={activeItem === index ? 'active' : ''}
                        key={`${el}_${index}`}
                        onClick={() => onSelectItem(index)}
                    >{el}</li>
                })
                }
            </ul>
        </div>
    )

}

export default Categories;

import React from "react";

import classnames from "classnames";

const Button = ({className, onClick, children, outline}) => {

    return (
        <button
            onClick={onClick}
            className={classnames('button', className, {
                'button-outline': outline,
            })}
        >
            {children}
        </button>
    )
}

export default Button;
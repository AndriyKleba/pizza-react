import React from "react";
import PropTypes from "prop-types";

import classnames from "classnames";

function PizzasItem({imageUrl, name, types, sizes, price, category, rating}) {

    const [activeType, setActiveType] = React.useState(types[0]);
    const typeStructure = ['тонке', 'традиційне'];

    const [activeSize, setActiveSize] = React.useState(sizes[0]);
    const sizesStructure = [26, 30, 40];


    const onSelectType = (index) => {
        setActiveType(index)
    };

    const onSelectSizes = (index) => {
        setActiveSize(index)
    };

    return (
        <div className="pizza-block">
            <img
                className="pizza-block__image"
                src={imageUrl}
                alt="Pizza"
            />
            <h4 className="pizza-block__title">{name}</h4>
            <div className="pizza-block__selector">
                <ul>
                    {typeStructure.map((type, index) => (
                        <li
                            key={type}
                            className={classnames({
                                'active': activeType === index,
                                'disabled': !types.includes(index)
                            })}
                            onClick={() => onSelectType(index)}
                        >{type}</li>
                    ))}
                </ul>
                <ul>
                    {sizesStructure.map((size, index) => (
                        <li
                            key={size}
                            className={classnames({
                                'active': activeSize === index,
                                'disabled': !sizes.includes(size)
                            })}
                            onClick={() => onSelectSizes(index)}
                        >{size} см.</li>
                    ))}
                </ul>
            </div>
            <div className="pizza-block__bottom">
                <div className="pizza-block__price">від {price} грн</div>
                <div className="button button--outline button--add">
                    <svg
                        width="12"
                        height="12"
                        viewBox="0 0 12 12"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                            fill="white"
                        />
                    </svg>
                    <span>Добавити</span>
                    <i>2</i>
                </div>
            </div>
        </div>
    )
}

PizzasItem.propTypes = {
    imageUrl: PropTypes.string,
    name: PropTypes.string,
    types: PropTypes.arrayOf(PropTypes.number),
    sizes: PropTypes.arrayOf(PropTypes.number),
    price: PropTypes.number,
    category: PropTypes.number,
    rating: PropTypes.number,
}

PizzasItem.defaultProps = {
    imageUrl: 'Немає забраження',
    name: 'Тест',
    price: 0,
    types: [],
    sizes: [],
    category: 0,
    rating: 0
}

export default PizzasItem;
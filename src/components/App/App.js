import React from "react";
import {Route} from "react-router-dom";
import axios from "axios";
import {useDispatch} from "react-redux";

import Header from "../Header";
import Home from "../Home";
import Cart from "../Cart";

import {setPizzas} from "../../redux/actions/pizzas";

import '../../scss/app.scss';


function App() {
    const dispatch = useDispatch();

    React.useEffect(() => {
        axios('http://localhost:3000/db.json').then(({data}) => {
            dispatch(setPizzas(data.pizzas));
        })
    }, []);

    return (
        <div className="wrapper">
            <Header/>
            <div className="content">
                <Route path="/" component={Home} exact/>
                <Route path="/cart" component={Cart} exact/>
            </div>
        </div>
    )
}

export default App;

// function mapStateToProps(state) {
//     return {
//         items: state.pizzas.items,
//         filters: state.filters
//     }
// }
//
// function mapDispatchToProps(dispatch) {
//     return {
//         setPizzas: (items) => dispatch(setPizzasAction(items))
//     }
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(App);

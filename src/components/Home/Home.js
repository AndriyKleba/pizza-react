import React from "react";

import Categories from "../Categories";
import SortCategoryItem from "../SortCategoryItem";
import PizzasItem from "../PizzasItem";
import {useDispatch, useSelector} from "react-redux";
import {setCategory} from "../../redux/actions/filter"


function Home() {
    const dispatch = useDispatch();
    const items = useSelector(({pizzas}) => pizzas.items);

    return (
        <div className="container">
            <div className="content__top">
                <Categories
                    onClickItem={(idx) => dispatch(setCategory(idx))}
                    items={[
                        'М\'ясні',
                        'Вегетаріанська',
                        'Гриль',
                        'Гострі',
                        'Закриті'
                    ]}/>
                <SortCategoryItem sortItems={[
                    {name: 'популярності', type: 'popular'},
                    {name: 'ціною', type: 'price'},
                    {name: 'алфавітом', type: 'alphabet'}
                ]}/>
            </div>
            <h2 className="content__title">Всі піци</h2>
            <div className="content__items">
                {
                    items && items.map(item => (
                        <PizzasItem key={item.id} {...item}/>
                    ))
                }
            </div>
        </div>
    )
}

export default Home;

